<?php

namespace wms\traits;

use Yii;

trait ApplicationTrait
{
    /**
     * @inheritdoc
     */
    protected function bootstrap()
    {
        if ($this->extensions === null) {
            $yiiExtFile = Yii::getAlias('@vendor/yiisoft/extensions.php');
            $wmsExtFile = Yii::getAlias('@vendor/filsh/extensions.php');
            
            $packageDataProvider = 
            
            $this->extensions = array_merge(
                is_file($yiiExtFile) ? include($yiiExtFile) : [],
                is_file($wmsExtFile) ? include($wmsExtFile) : []
            );
        }
        parent::bootstrap();
    }
    
    /**
     * @inheritdoc
     */
    public function coreComponents()
    {
        return array_merge(parent::coreComponents(), [
            'packageManager' => [
                'class' => \wms\base\PackageManager::class
            ],
        ]);
    }
    
    /**
     * Returns the packageManager component.
     * @return \wms\base\PackageManager the packageManager component.
     */
    public function getPackageManager()
    {
        return $this->get('packageManager');
    }
}
<?php

namespace wms\web;

use wms\traits\ApplicationTrait;

class Application extends \yii\web\Application
{
    use ApplicationTrait;
}
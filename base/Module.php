<?php

namespace wms\base;

use Yii;
use yii\i18n\PhpMessageSource;

class Module extends \yii\base\Module
{
    /**
     * @var string The prefix for module tables.
     *
     * @see [[yii\db\Connection::tablePrefix]]
     */
    public $tablePrefix;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        
        // register dependency for access to instance by dependency injection container
        Yii::$container->setSingleton(get_called_class(), $this);
        
        if(!isset(Yii::$app->i18n->translations['wms/*'])) {
            Yii::$app->i18n->translations['wms/*'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
            ];
        }
    }
    
    /**
     * Translates a message to the specified language.
     * 
     * @See [[Yii::t()]]
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('wms/' . $category, $message, $params, $language);
    }
    
    public static function tablePrefix($name)
    {
        if(($module = Yii::$container->get(get_called_class())) === null || $module->tablePrefix === null) {
            return $name;
        }
        
        if (strpos($name, '{{') !== false) {
            $name = preg_replace('/\\{\\{(.*?)\\}\\}/', '\1', $name);

            return '{{' . str_replace('%', '%' . $module->tablePrefix, $name) . '}}';
        } else {
            return $name;
        }
    }
}
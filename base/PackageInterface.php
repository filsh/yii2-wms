<?php

namespace wms\base;

interface PackageInterface
{
    const TYPE_REMOTE = 'remote';
    const TYPE_LOCAL = 'local';
}
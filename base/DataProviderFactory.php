<?php

namespace wms\base\components;

use Yii;
use wms\base\package\DataProvider as PackageDataProvider;

class DataProviderFactory
{
    public function getPackageDataProvider(array $params = [])
    {
        return Yii::$container->get(PackageDataProvider::class, [], $params);
    }
}
<?php

namespace wms\console;

use wms\traits\ApplicationTrait;

class Application extends \yii\console\Application
{
    use ApplicationTrait;
}